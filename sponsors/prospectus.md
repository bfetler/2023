---
title: Sponsorship Prospectus
---

## So you want to sponsor {{ site.data.event.name }}...

You're awesome!

## Quick Facts

- PyGotham's in-person conferences sell out every year. This year, PyGotham is
  fully online and free to attend, and we expect our largest audience ever in
  2023.

- The schedule contains two full days of talks and short films, an attendee chat
  server, and other uniquely remote content. Talks will cover topics including
  core Python, data science, web development, and more. For an idea of what to
  expect, take a look at [this year's talk list]({% link talks/index.md %}), and
  see previous years' talk lists for linked videos:
  [2021](https://2021.pygotham.org/talks/),
  [2020](https://2020.pygotham.org/talks/),
  [2019](https://2019.pygotham.org/talks/),
  [2018](https://2018.pygotham.org/talks/),
  [2017](https://2017.pygotham.org/talks/),
  [2016](https://2016.pygotham.org/talks/),
  [2015](https://2015.pygotham.org/talks/),
  [2014](https://2014.pygotham.org/talks.html).

- PyGotham strives to be accessible to everyone. PyGotham is presented with
  English captions and American Sign Language interpreting.

In order to keep the conference free for everyone, we need your help.
Several different levels of sponsorship are available. If you have any
questions, you can contact us directly.

Once you're ready, go ahead and send an email to
[sponsors@pygotham.org](mailto:sponsors@pygotham.org).

## Corporate Sponsorship Levels

| Benefit                                                    | Platinum  | Gold       | Silver   |
|                                                            | $6,000    | $3,000     | $1,500   |
| :--------------------------------------------------------: | :-------: | :--------: | :------: |
| Limited to                                                 | 4         | ∞          | ∞        |
| Logo on video bumper                                       | ✅        |            |          |
| Video ads between conference sessions                      | ✅        | ✅         |          |
| Access to the PyGotham chat server during the conference   | ✅        | ✅         | ✅       |
| Access to opt-in attendee contact information sharing      | ✅        | ✅         | ✅       |
| Logo and text in conference media                          | ✅        | ✅         | ✅       |
| Announcements in social media                              | ✅        | ✅         | ✅       |
| Priority access to PyGotham 2024 sponsorship opportunities | ✅        | ✅         | ✅       |
{: .table.table-striped.table-bordered }

### ASL interpreting and English language captioning sponsorships

À la carte add-on sponsorship options to support PyGotham's ASL interpreting and
English language captioning offerings is available upon request. If you would like
to add these options to your standard sponsorship package, please let us know in
your sponsorship inquiry email.

## FAQ

### Does PyGotham offer Community and Open Source sponsorships?
Absolutely! Gold tier and silver tier equivalent sponsorships are available. If
your organization or project is part of the Python community (or would like to
be), reach out to [sponsors@pygotham.org](mailto:sponsors@pygotham.org).

### What are sponsor video ads?
To stay true to the network television experience, PyGotham TV will air ads
between conference talks. Examples include product demos, testimonials, and even
infomercials; fun and creativity are encouraged. Note: all ads must be approved
by conference staff prior to the event.

### How does contact information sharing work?

Per PyGotham's [privacy policy]({% link about/privacy-policy.md %}), PyGotham does
not share attendee information with third parties by default. The
attendee-sponsor relationship is a key aspect of the conference experience, so
PyGotham is providing attendees a form to opt into sharing their contact
information with conference sponsors.
