---
duration: 28
presentation_url:
room:
slot: 2023-10-06 12:35:00-04:56
speakers:
- Peter Vidos
title: Let them explore! Building interactive, animated reports in Streamlit with
  ipyvizzu & a few lines of Python
type: talk
video_url:
---

It's great when you can share the results of your analysis not only as a
presentation but as something that non-data scientists can explore on their
own, looking for insights and applying their business expertise to
understand the significance of what they find.

With its accessibility for both creators and viewers,
[Streamlit](https://streamlit.io) offers a brilliant platform for data
scientists to build and deploy data apps. Now, with the integration of
[ipyvizzu](https://ipyvizzu.vizzuhq.com/latest/) - a new, open-source data
visualization tool focusing on animation and storytelling - you can quickly
create and publish interactive, animated reports and dashboards on top of
static or dynamic data sets and your models.

In this talk, **one of the creators of ipyvizzu shows how their technology
works within Streamlit and the advantages of using animation in self-service
data exploration** to help business stakeholders feel smarter and do a
better job.
