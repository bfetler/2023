---
duration: 14
presentation_url:
room:
slot: 2023-10-07 10:10:00-04:56
speakers:
- Mariatta
title: PEP talk
type: talk
video_url:
---

You've heard of PEP 8. But do you know that there are other PEPs too. PEPs
are not about style guide!

Learn about what PEPs are, the role of PEPs in core Python, and how you can
go about writing your own PEP.
