---
duration: 14
presentation_url:
room:
slot: 2023-10-06 14:25:00-04:56
speakers:
- Ciprian Stratulat
title: How (and Why) to Chain Generators in Python
type: talk
video_url:
---

You already know that Python generators are useful little tools. In this
short talk, I will show you how to put several generators (and other
iterators) in an elegant, Pythonic tool belt through chaining.
