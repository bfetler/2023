---
duration: 10
presentation_url:
room:
slot: 2023-10-07 15:35:00-04:56
speakers:
- Andrew Knight
title: Awesome Web Testing with Playwright
type: talk
video_url:
---

Playwright is a fantastic new open source web testing framework from
Microsoft. With Playwright, you can automate tests for web UIs and APIs that
run on multiple browsers without flaky failures. It's a nice, modern
alternative to Selenium WebDriver, and you can use Playwright in plain-old
Python!

In this concise 10-minute quickstart, we'll learn how to configure a Python
project with Playwright and pytest, automate a basic web test, and run it
against different browsers. You'll be able to add more tests to our starter
project on your own after the talk!
