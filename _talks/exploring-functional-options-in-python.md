---
duration: 15
presentation_url:
room:
slot: 2023-10-06 14:40:00-04:56
speakers:
- Rami Awar
title: Exploring Functional Options in Python
type: talk
video_url:
---

Documenting lots of arguments *well* is hard. Validating each is even
harder. Function arguments are not scalable past some point. Configuration
structs are heavy, specially if every function expected it's own config
instance. Functional APIs solve this problem in their own awkward little way
with a bit more code.

The first time I saw functional options in Golang, I didn't fully understand
why something as simple as configuring options (class or function args)
looked so awkward. Then I read about the technique and realized that this
solved a problem we have in every language, not just Golang.

Several years later, I found the chance to apply it when designing an API in
Python and noticed no existing implementations! This talk is about adopting
a very interesting API option design from Golang's Rob Pike.
