---
duration: 29
presentation_url:
room:
slot: 2023-10-07 13:15:00-04:56
speakers:
- Joshua Cannon
title: A BuildEngineer in a buildless lang
type: talk
video_url:
---

There’s more to Python than the language. And there’s more to Build
Engineering than building code.

An entire ecosystem of amazing tools sit just beyond the atmosphere of
Python’s interpreter. It’s a Build Engineer’s job to ensure you’re using the
right ones, configured the right way, and that they run cohesively. 
As IBM Watson Orders' Build Engineer I am the guardian of the developer
experience of almost 100 engineers and data scientists in a Python monorepo.
In the past year pre-submit time plummeted, the ease-of-use of local
development tools skyrocketed, and I became a maintainer of an Open Source
Build System. I am restlessly passionate about their developer experience as
well as yours. You should be too.

Come find out why Build Engineering is important to you and your org,
through one person’s constant struggle to expect better, how to find and
foster Build Engineering, and how you can build better Python for yourself
and others.
