---
duration: 26
presentation_url:
room:
slot: 2023-10-07 15:10:00-04:56
speakers:
- Owein Reese
title: Stop writing so many darn logs
type: talk
video_url:
---

Only a lumberjack could be proud of the number of logs we, as an industry,
generate. Let's not be the next company to be hit with a $65M bill for
logging. We all need to get smart, tactical even, about the way we craft our
logs so that they aren't just another thing like comments in code. When we
log, it should become a potent force for good, reducing incident recovery
times, alleviating the support burden many of us face and making all the new
fancy vendor AI/Observabilty offerings easier to integrate. Come spend a few
minutes of your life listening to me tell war stories involving the humble
logging statement.
