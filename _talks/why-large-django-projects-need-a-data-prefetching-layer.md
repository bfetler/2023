---
duration: 24
presentation_url:
room:
slot: 2023-10-07 14:20:00-04:56
speakers:
- Flávio Juvenal
title: Why large Django projects need a data (prefetching) layer
type: talk
video_url:
---

Django REST Framework focus on Don’t Repeat Yourself is useful for code
simplicity and compatibility with built-in solutions for permissions,
pagination, filters, etc. However, after projects grow in complexity, DRF’s
default architecture isn’t enough to ensure code maintainability. Often, any
change requires navigating through a lot of nesting to ensure all necessary
ORM calls and avoid serious performance slowdowns. In this talk, you’ll
learn how to use a custom data prefetch layer to avoid those issues by
gathering together code that changes together.
