---
duration: 14
presentation_url:
room:
slot: 2023-10-06 15:55:00-04:56
speakers:
- Indranil Ghosh
title: 'Python: A career changing/shaping language'
type: talk
video_url:
---

Python is ubiquitous. If your day job is in any kind of scientific field,
you might have had an encounter with Python, no matter how minuscule. For
me, who is currently a mathematics Ph.D. student, Python has been a
significant basis for my entire journey. This is more of a story than a
talk. A story of how Python has been shaping my career trajectory since my
undergraduate days, which started out with a major in Physics.
