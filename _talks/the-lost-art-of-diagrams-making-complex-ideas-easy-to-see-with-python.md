---
duration: 28
presentation_url:
room:
slot: 2023-10-06 12:05:00-04:56
speakers:
- Tadeh Hakopian
title: 'The Lost Art of Diagrams: Making Complex Ideas Easy to See with Python'
type: talk
video_url:
---

This talk is about communicating with visuals to make complex ideas simple
to understand. Over the years I have produced diagrams, charts,
illustrations and other graphics to help people understand sophisticated
project concepts. This includes project architecture, standard operating
procedures, coding concepts, sprints and roadmaps.

You will be guided through ways of how to make stylized examples of your
project code and workflows in easy to follow examples. By using common
software for illustrations along with some simple guidelines you too can
make easy to follow visual content for your next project.
