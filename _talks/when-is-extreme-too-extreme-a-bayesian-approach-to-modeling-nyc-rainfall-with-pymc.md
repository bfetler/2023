---
duration: 11
presentation_url:
room:
slot: 2023-10-06 13:45:00-04:56
speakers:
- Jorn Mossel
title: When is extreme too extreme? A Bayesian approach to modeling NYC rainfall with
  PyMC
type: talk
video_url:
---

On September 1st 2021, hurricane Ida hit NYC, causing extreme rainfall
resulting in deadly flash floods. The weather station at Central Park
recorded its wettest hour in history with 3.15 inches of rain, shattering
the previous record. One might wonder: is such an extreme event
statistically expected to happen once in a while (say every 100 years), or
is this an indication of a changing climate? In this talk we take a purely
statistical approach to tackle this question. We start with the introduction
of Extreme Value Theory, a branch of statistics devoted to extreme events
such as floods, stock market crashes and reliability engineering. We apply
this to NYC rainfall data with a simple python implementation. Next, we
switch gears and use a Bayesian approach with PyMC, to incorporate data from
nearby weather stations for better accuracy. Along the way we get a quick
intro of hierarchical models and Gaussian processes.
