---
duration: 20
presentation_url:
room:
slot: 2023-10-07 11:15:00-04:56
speakers:
- Jay Miller
title: Video Game-ifying Your Learning Processes
type: talk
video_url:
---

Developers of all skill levels spend their a good portion of time learning
new techniques and modules and applying what they've learned. Many
developers look to gaming as a break away from the code, but there is much
that can be learned with your hands on the controller.

This talk will explore common video game archetypes, their techniques, and
situations in Python development where those techniques can be used to
further your personal development.
