---
duration: 11
presentation_url:
room:
slot: 2023-10-07 11:35:00-04:56
speakers:
- Anay Pant
title: Powering Youth Civic Engagement and Social Impact with Python data analytics.
type: talk
video_url:
---

In Fall 2022, I enrolled in American Politics class as a Junior in High
School. The course taught political science theories e.g.  American
Constitution, civil rights, civil liberties, Presidency, Congress, political
parties, elections, public opinion, media, advertising, social identity,
race and ethnic politics and political polling.

Inspired, I participated as a student clerk in a county polling station in
midterms 2022.  I surveyed voters, with the goal to understand how folks in
my community perceive civic engagement.

My peers and I will be ready to vote in a few years, but are we informed and
aware?  I found that youth are getting drowned in plethora of information
via social media e.g videos, tweets, messaging apps etc, which can be
superficial and likely from untrusted news sources.

In order to engage youth in a meaningful way, I launched a novel news
platform. This platform provides AI powered news apps (iOS and Android) to
youth where they can quickly read smart summaries of important issues from
trusted news sources and comment /vote on them.  The aggregated analytics
are shared with elected leaders. So youth can get their voices heard and
hopefully be motivated to vote!

So far we have 20 subscribers with daily app usage and engagement via votes
and comments.  The feedback has been positive and encouraging.

Anonymized data gets analyzed daily via pipelines built using Python data
analysis tools (Deepnote notebooks, pandas). This data is being shared with
subscribers, elected officials and the general public providing an insight
into the thoughts of our future citizens and voters.
