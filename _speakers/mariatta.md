---
name: Mariatta
talks:
- "PEP talk"
---
Mariatta is a Python Core Developer and a co-organizer for PyLadies
Vancouver. In her free time, she contributes to open source, builds GitHub
bots, fixes typos, and likes to tell you about f-strings. Her favorite emoji
is 😝.
