---
name: Flávio Juvenal
talks:
- "Why large Django projects need a data (prefetching) layer"
---
I'm Flávio, a software engineer from Brazil and Chief Scientist at Vinta
Software (www.vinta.com.br). I’ve been building web products with Python and
Django for the last 13 years. I love drinking medium and light roast coffee
and visiting museums around the world. Recently I got into retrogaming and
I've been (trying) to fix and play SNES and Genesis consoles.
