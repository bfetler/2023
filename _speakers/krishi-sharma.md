---
name: Krishi Sharma
talks:
- "Brink: Saving a Society from Starvation"
---
Krishi Sharma is a software developer at KUNGFU.AI where she builds software
applications that power machine learning models and deliver data for a broad
range of services. As a former data scientist and machine learning engineer,
she is passionate about building tools that ease the infrastructure
dependencies and reduce potential technical debt around handling data. She
helped build and maintains an internal Python tool, Potluck, which allows
machine learning engineers the ability to bootstrap a containerized,
production ready application with data pipelining templates so that her team
can focus on the data and metrics without squandering too much time
finagling with deployment and software.
