---
name: Anay Pant
talks:
- "Powering Youth Civic Engagement and Social Impact with Python data analytics."
---
I am currently a High School Junior at The Athenian School in Danville,
California.  I have an avid interest in Robotics , Coding (Python, Java),
AI/ML , Social Justice & Civic Engagement and playing video games! I love
soccer and basketball too.  You can learn more about my projects at
https://www.linkedin.com/in/anay-pant/
