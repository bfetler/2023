---
name: Julian Berman
talks:
- "Open Source: from Passion to Hobby to Work, or There and Back Again"
---
Julian Berman is a software developer and open source software maintainer
who currently serves as a Technical Lead, API Specifications at Postman,
where he is graciously funded to work on open source projects, most notably
the JSON Schema specification(s) and his implementation of them in Python.
He previously served as Vice President of Engineering for Decisioning
Automation at Deloitte Digital, overseeing teams of engineers and data
scientists delivering machine learning solutions to large clients.
