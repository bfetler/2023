---
title: What is PyGotham?
---

PyGotham is a New York City based, eclectic, Py-centric conference covering many
topics. There's a diverse speaker list, and some things which will be quite
different. PyGotham attracts developers of various backgrounds and skill levels
from the New York metropolitan area and beyond.

## History
PyGotham began in 2011 and was comprised largely of the NYC Python community.
Since then, the conference has taken place (almost) every year and grown
significantly. While the conference has gotten larger, PyGotham has always been
and remains a conference for developers and run by developers.

## Who?
PyGotham is a production of [Big Apple Py](https://bigapplepy.org) and made
possible by a very generous and overworked group of volunteers. To reach them,
send a message to [organizers@pygotham.org](mailto:organizers@pygotham.org).
