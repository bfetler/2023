---
name: PyTexas
tier: community
site_url: https://pytexas.org/
logo: pytexas.svg
twitter: pytexas
---
The PyTexas Foundation is responsible for fostering education and community
around the Programming language within Texas. PyTexas hosts an annual conference
in the Spring, with next year's conference being April 19 - 21, 2024.
