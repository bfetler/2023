module Jekyll
  module CopyWithDelay
    def copy_with_delay(talks)
      delayed_talks = []
      talks.each do |talk|
        next if talk.data["type"] == "end-of-day"
        delayed_talk = talk.clone
        delayed_talk.instance_variable_set(:@data, delayed_talk.data.clone)
        if not delayed_talk.data["slot"].nil?
          delayed_talk.data["slot"] += 60*60*8  # Add 8 hours in seconds
        end
        delayed_talks << delayed_talk
      end
      return delayed_talks
    end
  end
end

Liquid::Template.register_filter(Jekyll::CopyWithDelay)
